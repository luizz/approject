#include-once 

Global Const $DIR_PROJETO = @ScriptDir

Global $projetoRoot[1], $projetoMonitor[1]

Func _initProjetos()
	
	Local $buscar_proj = _findFiles($DIR_PROJETO)
	Local $countProjetos = 0;
	
	If $buscar_proj <> 0 Then
			While 1
				
			   $projeto_dir = FileFindNextFile($buscar_proj)
				
			   If @error Then 
					ExitLoop 
			   EndIf
				 
			   ;Local $is_dir = DirGetSize($projeto_dir)
			   
			   ;If Not @error Then
				    
					Local $newProjeto[7], $update, $commit, $info, $open, $monitor, $web_projeto
					
					$projeto_menu = TrayCreateMenu($projeto_dir, $projetoitem)

					$open	= TrayCreateItem("Abrir", $projeto_menu)
					TrayItemSetOnEvent($open ,"_abrirProjeto")
					
					$web_projeto	= TrayCreateItem("Web Projeto", $projeto_menu)
					TrayItemSetOnEvent($web_projeto ,"_defaultProjeto")
					
					If FileExists($projeto_dir & "/.svn") Then
						
						$update	= TrayCreateItem("SVN Update", $projeto_menu)
						$commit	= TrayCreateItem("SVN Commit", $projeto_menu)
						$info	= TrayCreateItem("SVN Info", $projeto_menu)
						$monitor = TrayCreateItem("SVN Monitorar", $projeto_menu)
						
						TrayItemSetOnEvent($update ,"_svnUpdate")	
						TrayItemSetOnEvent($commit ,"_svnCommit")
						TrayItemSetOnEvent($info ,"_infoProjeto")
						TrayItemSetOnEvent($monitor ,"_monitorarProjeto")
						
					 EndIf
					 
					Local $favicon_busca = "", $favicon_type
					
					If FileExists($projeto_dir & "/_imagens/site/favicon.png") Then
						$favicon_type = 0
						$favicon_busca = $projeto_dir & "/_imagens/site/favicon.png"
					ElseIf FileExists($projeto_dir & "/_imagens/favicon.png") Then
						$favicon_type = 0
						$favicon_busca = $projeto_dir & "/_imagens/favicon.png"
					ElseIf FileExists($projeto_dir & "/favicon.png") Then
						$favicon_type = 0
						$favicon_busca = $projeto_dir & "/favicon.png"
					ElseIf FileExists($projeto_dir & "/_imagens/site/favicon.ico") Then
						$favicon_type = 1
						$favicon_busca = $projeto_dir & "/_imagens/site/favicon.ico"
					ElseIf FileExists($projeto_dir & "/_imagens/favicon.ico") Then
						$favicon_type = 1
						$favicon_busca = $projeto_dir & "/_imagens/favicon.ico"
					ElseIf FileExists($projeto_dir & "/favicon.ico") Then
						$favicon_type = 1
						$favicon_busca = $projeto_dir & "/favicon.ico"
					EndIf
					
					If $favicon_busca <> "" And FileExists($favicon_busca) Then

					    $favicon = _LoadImage($favicon_busca, $favicon_type)
						_TrayMenuAddIcon($favicon, $countProjetos, $projetoitem)
						_IconDestroy($favicon)

				    EndIf
	
					$newProjeto[0] = $projeto_dir
					$newProjeto[1] = $update
					$newProjeto[2] = $commit
					$newProjeto[3] = $open
					$newProjeto[4] = $info
					$newProjeto[5] = $monitor
					$newProjeto[6] = $web_projeto
					
					_pushArray($newProjeto, $projetoRoot)
					
					$countProjetos = $countProjetos + 1
				 ;EndIf
				 				 
			WEnd
	EndIf
	
EndFunc

Func _obterProjeto($op, $state = 0)
	
	If Int($state) <> 0 Then 
		TrayItemSetState(@TRAY_ID,$state)
	EndIf
	
	For $i = 0 to UBound($projetoRoot) - 1
			If IsArray($projetoRoot[$i]) Then
				Local $getProjeto = $projetoRoot[$i]
				If _inArray($op, $getProjeto) == True Then
					Return $getProjeto[0]
				EndIf
			EndIf
		Next
EndFunc

Func _abrirProjeto()

	$dir = _obterProjeto(@TRAY_ID, 4)
	
	; Abrir diretorio do projeto ou arquivo
	Run("explorer.exe " & $dir)
	
 EndFunc
 
 Func _defaultProjeto()

    $dir = _obterProjeto(@TRAY_ID, 4)
	
	_log( "Approject Webserver path dir em " & @ScriptDir & "\" & $dir & @CRLF)
	
	$projetoDefault = @ScriptDir & "\" & $dir
	
EndFunc

Func _adminProjeto()
		
EndFunc
	
Func _atualProjeto()
	
EndFunc

Func _infoProjeto()
	
	$dir = _obterProjeto(@TRAY_ID, 4)
	
	Local $info_online = "", $info_local = "", $url_online = ""
	
	$info_local = _svnInfo($dir, $DIR_PROJETO & "/");
	
	$url_online = _svnInfo($dir, $DIR_PROJETO & "/", "URL");

	$info_online = _svnInfo($url_online & "/", False);

	MsgBox(0, "Info", "Online : " & @LF & $info_online &  @LF &" Local: " & @LF & $info_local)
	
EndFunc

Func _monitorarProjeto()
   
	$dir = _obterProjeto(@TRAY_ID)
	$status = TrayItemGetState(@TRAY_ID)

	If $status == 68 Then
		TrayItemSetState(@TRAY_ID,4)

		For $i = 0 to UBound($projetoMonitor) - 1
		
			If IsArray($projetoMonitor[$i]) Then
				
				Local $monitor = $projetoMonitor[$i]
		 
				if $monitor[0] == $dir Then
					
					$projetoMonitor[$i] = False
				EndIf
				
			EndIf
		
		Next
		
	ElseIf $status == 65 Then
				
		Local $monitor[3]
		
		$monitor[0] = $dir
		$monitor[1] = False
		$monitor[2] = False

		_pushArray($monitor, $projetoMonitor)
	EndIf
EndFunc
 
Func _alteradoProjeto()
	
	For $i = 0 to UBound($projetoMonitor) - 1
		
		If IsArray($projetoMonitor[$i]) Then
			
		 Local $monitor = $projetoMonitor[$i]
		 
		 if $monitor[1] == False Or $monitor[2] == False Then
			 
			 Local $url_online = _svnInfo($monitor[0], $DIR_PROJETO & "/", "URL")
			 
			 _pushArray($url_online, $monitor)
			 
			 $monitor[1] = _svnInfo($monitor[0], $DIR_PROJETO & "/", "Revision")
			 
			 $monitor[2] = _svnInfo($url_online & "/", False,  "Revision")
			 
			 $projetoMonitor[$i] = $monitor
		 Else
			 
			 $revision = _svnInfo($monitor[3] & "/", False,  "Revision")
			 
			 If $monitor[2] <> $revision Then
				 
				 $projeto = _svnInfo($monitor[0], $DIR_PROJETO & "/",  "Path")
				 $autor = _svnInfo($monitor[3] & "/", False,  "Last Changed Author")
				 
				 $monitor[2] = $revision
				 
				 $projetoMonitor[$i] = $monitor
				 
				 TrayTip(StringUpper($projeto) & " Alterado", StringUpper($autor) & " Alterou ", 5, 1)
			EndIf
			 
			ConsoleWrite("Projeto: "&$monitor[0]&" Local: " & $monitor[1] &" Online: "& $monitor[2] & @LF)
			Sleep(300)
			
		 EndIf
		 
	 EndIf
	 
	Next

EndFunc

Func _novoProjeto()
   
EndFunc