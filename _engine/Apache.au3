#include-once
Global Const $DIR_APACHE = @HomeDrive & "Apache24"
Global Const $SER_APACHE = "apache2.2";

Func _apacheHost()

	TrayItemSetState(@TRAY_ID,4)
	Run(@WindowsDir & "\Notepad.exe " & $DIR_APACHE & "\conf\extra\httpd-vhosts.conf", "", @SW_MAXIMIZE)
EndFunc

Func _apacheStop()

	Local $oShell = ObjCreate("shell.application")

	TrayItemSetState(@TRAY_ID,4)
	if $oShell.IsServiceRunning($SER_APACHE) then

		$oShell.ServiceStop($SER_APACHE,false)

		TrayTip("", "Apache Stop", 15)
	endif
EndFunc

Func _apacheStart()

	Local $oShell = ObjCreate("shell.application")

	TrayItemSetState(@TRAY_ID,4)
	if $oShell.IsServiceRunning($SER_APACHE) then

		$oShell.ServiceStop($SER_APACHE,false)
		TrayTip("", "Apache Stop", 15)
		$oShell.ServiceStart($SER_APACHE,false)
		TrayTip("", "Apache Start", 15)

	Else
		$oShell.ServiceStart($SER_APACHE,false)
		TrayTip("", "Apache Start", 15)
	endif
EndFunc