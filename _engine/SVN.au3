#include-once 
Global $svnInfo[2]

Func _svnUpdate()
	
	TrayItemSetState(@TRAY_ID,4)
	$dir = _obterProjeto(@TRAY_ID)
	Run("C:\Program Files\TortoiseSVN\bin\TortoiseProc.exe  /command:update /path:"& $dir &"/", "", @SW_MAXIMIZE)
	
EndFunc

Func _svnCommit()
	
	TrayItemSetState(@TRAY_ID,4)
	
	$dir = _obterProjeto(@TRAY_ID)
	Run("C:\Program Files\TortoiseSVN\bin\TortoiseProc.exe  /command:commit /path:"& $dir &"/", "", @SW_MAXIMIZE)

EndFunc

Func _svnInfo($dir, $path = False, $type = False)
	
	Local $line = "", $info = ""

		if $path == False Then
			
			if String($svnInfo[0]) <> $dir Then
				
				$svnInfo[0] = $dir
				$info = Run('svn info ' & $dir , @SystemDir, @SW_HIDE, 6)
			Else
				
				If $type <> False Then
				
					Return _svnInfoReplace($type, $svnInfo[1])
				Else
					
					Return $svnInfo[1]
				EndIf
				ConsoleWrite("CACHE INFO")
			EndIf
		Else
			
			if String($svnInfo[0]) <> String($path & $dir) Then
				
				$svnInfo[0] = $path & $dir
				$info = Run('svn info ' & $path & $dir , @SystemDir, @SW_HIDE, 6)
			Else
				If $type <> False Then
				
					Return _svnInfoReplace($type, $svnInfo[1])
				Else
					
					Return $svnInfo[1]
				EndIf
				ConsoleWrite("CACHE INFO")
			EndIf
		EndIf
		
	While 1
		$line = StdoutRead($info)
		If @error Then ExitLoop
			
		If String($line) <> "" Then

			$svnInfo[1] = $line
			
			ConsoleWrite(@LF &'svn info:' & $path & $dir& @LF)
			
			If $type <> False Then
				
				Return _svnInfoReplace($type, $line)
			Else
				
				Return $line
			EndIf
			
		EndIf
	Wend
	
EndFunc

Func _svnInfoReplace($type, ByRef $info)
	
	$infoSlipt = StringSplit($info, @LF)
				
		For $split = 0 to UBound($infoSlipt) - 1
					
			Local $infoLine = StringSplit($infoSlipt[$split] , ":")

			If String($type) == String($infoLine[1]) Then
						
				Return StringStripCR(StringTrimLeft($infoSlipt[$split], StringLen($type)+2))
			EndIf
		Next
	Return 0
	EndFunc