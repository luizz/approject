#include-once

HotKeySet("+!l", "_lipsum")

Func _pushArray(ByRef $add ,ByRef $array)
	If IsArray($array) Then
			Local $i = UBound($array)
			ReDim $array[$i + 1]
			$array[$i] = $add
	EndIf
EndFunc

Func _inArray(ByRef $isset, ByRef $array)
		If IsArray($array) Then
			For $i = 0 to UBound($array) - 1
				If VarGetType($array[$i]) == VarGetType($isset) And $array[$i] == $isset Then
					Return True;
				EndIf
			Next
		EndIf
	Return False
EndFunc

Func _hostWin()
	TrayItemSetState(@TRAY_ID,4)
	Run(@WindowsDir & "\Notepad.exe " & @WindowsDir &"\system32\drivers\etc\hosts ", "", @SW_MAXIMIZE)
EndFunc

Func lg($log)
	ConsoleWrite($log & @LF)
	EndFunc

Func _findFiles($dir, $busca = "*.*")

	FileChangeDir($dir)
	$search = FileFindFirstFile($busca)

	If $search = -1 Then
		Return 0
	Else
		Return $search
	EndIf

EndFunc

Func _trim($str)
	Local $string = String($str);
	;$string = StringTrimLeft( $string )
	;$string = StringTrimRight( $string )
	Return $string
EndFunc

Func _lipsum()

	ClipPut("Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sendo utilizado desde o s�culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu n�o s� a cinco s�culos, como tamb�m ao salto para a editora��o eletr�nica, permanecendo essencialmente inalterado. Se popularizou na d�cada de 60, quando a Letraset lan�ou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editora��o eletr�nica como Aldus PageMaker.");
EndFunc

Func _sobre()

	TrayItemSetState(@TRAY_ID,4)
	TrayTip("", "www.luizz.com.br", 5)
 EndFunc

 Func _log($log)

	TrayItemSetState(@TRAY_ID,4)
	TrayTip("", $log, 5)
 EndFunc

 Func _sair()
	 ;_cleanupSocket();
	 Exit
 EndFunc