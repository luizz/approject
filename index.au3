#cs ----------------------------------------------------------------------------
 AutoIt Version: 3.3.6.1
 Version: 1.0
 Author:  Luizz - www.luizz.com.br
 Script Function: Gerenciado de Virtual Host For Apache

#ce ----------------------------------------------------------------------------
#NoTrayIcon
#include "_engine/Util.au3"
#include "_engine/TrayMenuEx.au3"
#include "_engine/SVN.au3"
#include "_engine/Projeto.au3"
#include "_engine/Apache.au3"
;include "_engine/WebServer.au3"

Opt("TrayMenuMode",1)	; N�o aparecer default icone pause / exit
Opt("TrayOnEventMode",1)

Global $projetoitem = TrayCreateMenu("Projetos")
Global $projetoDefault = @ScriptDir; Diretorio padrao do webserve
Global $sIP = @IPAddress1 ; IP
Global $iPort = 80 ; Port
Global $sServerAddress = "http://" & $sIP & ":" & $iPort & "/" ; Endereco root

; Initialize a variable to represent a connection
;==============================================
Global $ConnectedSocket = -1
Global $iMainSocket

TrayCreateItem("")
$apache = TrayCreateMenu("Apache")
$ap_inciar_item	= TrayCreateItem("Iniciar", $apache)
TrayItemSetOnEvent($ap_inciar_item ,"_apacheStart")
$ap_parar_item	= TrayCreateItem("Para", $apache)
TrayItemSetOnEvent($ap_parar_item ,"_apacheStop")
$ap_hosts_item = TrayCreateItem("Hosts", $apache)
TrayItemSetOnEvent($ap_hosts_item ,"_apacheHost")

TrayCreateItem("")
$windows = TrayCreateMenu("Windows")
$win_hosts_item = TrayCreateItem("Hosts", $windows)
TrayItemSetOnEvent($win_hosts_item ,"_hostWin")

;TrayCreateItem("")
;$web_server = TrayCreateMenu("Web Server")
;$ws_on_item = TrayCreateItem("Ativar", $web_server)
;TrayItemSetOnEvent($ws_on_item ,"_webSocket")

TrayCreateItem("")
$aboutitem	= TrayCreateItem("Sobre")
TrayItemSetOnEvent($aboutitem ,"_sobre")

TrayCreateItem("")
$exititem	= TrayCreateItem("Sair")
TrayItemSetOnEvent($exititem ,"_sair")

_initProjetos()

TraySetClick(16)
TraySetIcon("favicon.ico")
TraySetToolTip("Gerenciador de Projetos" & @LF & " www.luizz.com.br ")

TraySetState()

;_alteradoProjeto()

While 1

Sleep(1000)
 ;_alteradoProjeto()

 ;if $ConnectedSocket <> -1 Then
;   _whileSocket()
; EndIf

WEnd

Exit